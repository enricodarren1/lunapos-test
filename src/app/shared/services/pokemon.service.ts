import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor() { }

  getListPokemon(url?: string) {
    return axios.get(url ? url : 'https://pokeapi.co/api/v2/pokemon?limit=25&offset=0');
  }

  getDetailPokemon(url: string) {
    return axios.get(url);
  }


}
