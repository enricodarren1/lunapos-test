import { Component, Input } from '@angular/core';

@Component({
  selector: 'poke-type',
  templateUrl: './poke-type.component.html',
  styleUrls: ['./poke-type.component.scss']
})
export class PokeTypeComponent {

  @Input() type: any;
  
  bgColor: string = "";

  constructor() { }

  ngOnInit() {
    this.setBackgroundColor();
  }


  setBackgroundColor() {
    if (this.type === 'water') {
      this.bgColor = '#46CBEC';
    } else if (this.type === 'rock') {
      this.bgColor = '#BC7A16'
    } else if (this.type === 'poison') {
      this.bgColor = '#8A68EC'
    } else if (this.type === 'ice') {
      this.bgColor = '#3F97FF'
    } else if (this.type === 'grass') {
      this.bgColor = '#53CDA8'
    } else if (this.type === 'fighting') {
      this.bgColor = '#EC8246'
    } else if (this.type === 'electric') {
      this.bgColor = '#ECC746'
    } else if (this.type === 'dark') {
      this.bgColor = '#727272'
    } else if (this.type === 'physic') {
      this.bgColor = '#C68FF1'
    } else if (this.type === 'normal') {
      this.bgColor = '#D2D2D2';
    } else if (this.type === 'ground') {
      this.bgColor = '#CD9C53';
    } else if (this.type === 'ghost') {
      this.bgColor = '#535FCD';
    } else if (this.type === 'fire') {
      this.bgColor = '#EC5046';
    } else if (this.type === 'fairy') {
      this.bgColor = '#F18FCA';
    } else if (this.type === 'bug') {
      this.bgColor = '#9DD06A';
    } else if (this.type === 'flying') {
      this.bgColor = '#C5F1FC';
    } else if (this.type === 'steel') {
      this.bgColor = '#D2D2D2';
    } else if (this.type === 'dragon') {
      this.bgColor = '#5056EC';
    }
  }

}
