import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokeTypeComponent } from './poke-type/poke-type.component';
import { HeaderComponent } from './header/header.component';

const components = [
  PokeTypeComponent,
  HeaderComponent
]

@NgModule({
  declarations: [components],
  imports: [
    CommonModule
  ],
  exports: [components]
})
export class ComponentsModule { }
