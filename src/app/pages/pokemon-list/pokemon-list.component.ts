import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/shared/services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent {

  pokemonList: any = [] || null;

  constructor(
    private readonly pokemonService: PokemonService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.getPokemonList();
  }

  getPokemonList(url?: string) {
    this.pokemonService.getListPokemon(url).then((res: any) => {
      this.pokemonList = res.data;
    });
  }

  gotoDetail(data: any) {
    this.router.navigate(['/pokemon-detail'], { state: { data } })
  }

}
