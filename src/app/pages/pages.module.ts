import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';
import { ComponentsModule } from '../shared/components/components.module';

const pages = [
  PokemonListComponent,
  PokemonDetailComponent
]

@NgModule({
  declarations: [
    pages
  ],
  imports: [
    CommonModule,
    ComponentsModule
  ],
  exports: [
    pages
  ]
})
export class PagesModule { }
