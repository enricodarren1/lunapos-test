import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from 'src/app/shared/services/pokemon.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent {

  state: any;
  dataDetails: any;
  selectedImage: string = "";

  constructor(
    private readonly router: Router,
    private readonly pokemonService: PokemonService
  ) {
    this.state = router.getCurrentNavigation()?.extras.state;
  }

  ngOnInit() {
    this.getDetailPokemon();
  }

  getDetailPokemon() {
    this.pokemonService.getDetailPokemon(this.state.data.url).then((res: any) => {
      this.dataDetails = res.data;
      this.selectedImage = res.data.sprites.front_default;
    });
  }

  goBack() {
    this.router.navigate(['/pokemon-list']);
  }

}
